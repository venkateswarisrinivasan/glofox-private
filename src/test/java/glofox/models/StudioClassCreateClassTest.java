package glofox.models;

import glofox.Exceptions.BookingNotValidException;
import glofox.Exceptions.StudioClassIsNotValidException;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudioClassCreateClassTest {

    @Test
    public void shouldCreateClass() {
        StudioClass mockClass = mock(StudioClass.class);
        when(mockClass.getId()).thenReturn(UUID.randomUUID());
        when(mockClass.isValid()).thenReturn(true);

        StudioClass.create(mockClass);

        Optional<StudioClass> studioClass = StudioClass.getScheduledClasses().stream().filter(s -> s.getId().equals(mockClass.getId())).findFirst();
        assertTrue(studioClass.isPresent());
        StudioClass scheduledClass = studioClass.get();
        assertEquals(mockClass.getClassName(), scheduledClass.getClassName());
        assertEquals(mockClass.getStartDate(), scheduledClass.getStartDate());
        assertEquals(mockClass.getEndDate(), scheduledClass.getEndDate());
        assertEquals(mockClass.getCapacity(), scheduledClass.getCapacity());
    }

    @Test(expected = StudioClassIsNotValidException.class)
    public void shouldNotScheduleClassAndThrowExceptionIfTheInputIsNotValid() {
        StudioClass mockClass = mock(StudioClass.class);
        when(mockClass.getId()).thenReturn(UUID.randomUUID());
        when(mockClass.isValid()).thenReturn(false);

        StudioClass.create(mockClass);

    }

    @Test
    public void shouldStudioClassBeValidIfItHasAllValues() {
        StudioClass newClass = StudioClass.builder().id(UUID.randomUUID())
                .className("class").startDate(new Date()).endDate(new Date()).capacity(10).build();

        assertTrue(newClass.isValid());

    }

    @Test
    public void shouldStudioClassNotBeValidIfItMissAnyValue() {
        StudioClass newClass = StudioClass.builder().build();
        assertFalse(newClass.isValid());

        newClass = StudioClass.builder().className("class").startDate(new Date()).endDate(new Date()).capacity(10).build();
        assertFalse(newClass.isValid());

        newClass = StudioClass.builder().id(UUID.randomUUID()).startDate(new Date()).endDate(new Date()).capacity(10).build();
        assertFalse(newClass.isValid());

        newClass = StudioClass.builder().id(UUID.randomUUID()).className("class").endDate(new Date()).capacity(10).build();
        assertFalse(newClass.isValid());

        newClass = StudioClass.builder().id(UUID.randomUUID())
                .className("class").startDate(new Date()).capacity(10).build();
        assertFalse(newClass.isValid());

        newClass = StudioClass.builder().id(UUID.randomUUID())
                .className("class").startDate(new Date()).endDate(new Date()).build();
        assertFalse(newClass.isValid());

    }

}