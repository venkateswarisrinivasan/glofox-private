package glofox.models;

import glofox.Exceptions.BookingNotValidException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class StudioClassMakeBookingTest {

    @Test
    public void shouldAddBooking() {
        UUID firstClassId = UUID.randomUUID();
        StudioClass firstClass = StudioClass.builder().id(firstClassId)
                .className("class").startDate(new Date()).endDate(new Date()).capacity(10).bookings(new ArrayList<>()).build();
        StudioClass.create(firstClass);

        UUID secondClassId = UUID.randomUUID();
        StudioClass secondClass = StudioClass.builder().id(secondClassId)
                .className("class").startDate(new Date()).endDate(new Date()).capacity(10).bookings(new ArrayList<>()).build();
        StudioClass.create(secondClass);

        String memberName = "memberA";
        Date bookingDate = firstClass.getStartDate();
        StudioClass.createBooking(firstClassId, bookingDate, memberName);

        assertEquals(1, firstClass.getBookings().size());
        assertEquals(0, secondClass.getBookings().size());

        Booking booking = firstClass.getBookings().get(0);
        assertEquals(bookingDate, booking.getBookingDate());
        assertEquals(memberName, booking.getMember());
    }

    @Test(expected = BookingNotValidException.class)
    public void shouldThrowExceptionIfBookingIsAfterEndDate(){
        UUID firstClassId=UUID.randomUUID();
        Date tenDaysFromNow=nDaysFromNow(10);
        StudioClass firstClass=StudioClass.builder().id(firstClassId)
                .className("class").startDate(new Date()).endDate(tenDaysFromNow).capacity(10).bookings(new ArrayList<>()).build();
        StudioClass.create(firstClass);

        String memberName="memberA";
        Date bookingDate=nDaysFromNow(11);
        StudioClass.createBooking(firstClassId,bookingDate,memberName);

        assertEquals(0,firstClass.getBookings().size());
    }

    @Test(expected = BookingNotValidException.class)
    public void shouldThrowExceptionIfBookingBeforeStartDate(){
        UUID firstClassId=UUID.randomUUID();
        Date tenDaysFromNow=nDaysFromNow(10);
        StudioClass firstClass=StudioClass.builder().id(firstClassId)
                .className("class").startDate(new Date()).endDate(tenDaysFromNow).capacity(10).bookings(new ArrayList<>()).build();
        StudioClass.create(firstClass);

        String memberName="memberA";
        Date bookingDate=nDaysFromNow(-1);
        StudioClass.createBooking(firstClassId,bookingDate,memberName);

        assertEquals(0,firstClass.getBookings().size());
    }


    private Date nDaysFromNow(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTime();
    }
}
