package glofox.controllers;

import glofox.controllers.HealthController;
import org.junit.Assert;
import org.junit.Test;

public class HealthControllerTest {
    @Test
    public void shouldReturnApplicationIsHealthy() {
        HealthController healthController = new HealthController();
        Assert.assertEquals("App is healthy", healthController.health());
    }
}