package glofox.controllers;

import glofox.Exceptions.StudioClassNotFoundException;
import glofox.models.Booking;
import glofox.models.StudioClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.UUID;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StudioClass.class})
public class BookingControllerTest {

    BookingController bookingController;

    @Before
    public void setup() {
        bookingController = new BookingController();
        PowerMockito.mockStatic(StudioClass.class);
    }

    @Test
    public void shouldReturn200IfTheBookingIsSuccessful() {
        Booking booking = Booking.builder().build();
        UUID classId = UUID.randomUUID();
        Date bookingDate = new Date();
        String member = "member";
        when(StudioClass.createBooking(classId, bookingDate, member)).thenReturn(booking);

        ResponseEntity<Booking> response = bookingController.book(classId, bookingDate, member);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        Assert.assertEquals(response.getBody(), booking);
    }

    @Test
    public void shouldReturn400IfTheClassIsScheduledSuccessfully() {
        when(StudioClass.createBooking(any(), any(), anyString())).thenThrow(StudioClassNotFoundException.class);

        ResponseEntity<Booking> response = bookingController.book(UUID.randomUUID(), new Date(), "a");

        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(response.getBody(), null);
    }

}