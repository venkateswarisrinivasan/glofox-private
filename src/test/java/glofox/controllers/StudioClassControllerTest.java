package glofox.controllers;

import glofox.Exceptions.StudioClassIsNotValidException;
import glofox.models.StudioClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StudioClass.class})
public class StudioClassControllerTest {

    StudioClassController studioClassController;

    @Before
    public void setup() {
        studioClassController = new StudioClassController();
        PowerMockito.mockStatic(StudioClass.class);
    }

    @Test
    public void shouldReturn200IfTheClassIsScheduledSuccessfully() {
        StudioClass mockStudio = mock(StudioClass.class);
        when(StudioClass.create(mockStudio)).thenReturn(mockStudio);

        ResponseEntity<StudioClass> response = studioClassController.create(mockStudio);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        Assert.assertEquals(response.getBody(), mockStudio);
    }

    @Test
    public void shouldReturn400IfTheClassIsScheduledSuccessfully() {
        PowerMockito.mockStatic(StudioClass.class);
        StudioClass mockStudio = mock(StudioClass.class);
        when(StudioClass.create(mockStudio)).thenThrow(StudioClassIsNotValidException.class);

        ResponseEntity<StudioClass> response = studioClassController.create(mockStudio);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(response.getBody(), null);
    }

}