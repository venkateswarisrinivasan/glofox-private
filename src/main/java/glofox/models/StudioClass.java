package glofox.models;

import glofox.Exceptions.BookingNotValidException;
import glofox.Exceptions.StudioClassIsNotValidException;
import glofox.Exceptions.StudioClassNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.*;

@Getter
@Builder
@AllArgsConstructor
public class StudioClass {

    @Getter
    private static List<StudioClass> scheduledClasses = new ArrayList<>();

    private UUID id;

    private String className;

    private Date startDate;

    private Date endDate;

    private int capacity;

    private List<Booking> bookings = new ArrayList<>();

    public StudioClass() {
        this.id = UUID.randomUUID();
    }

    public static StudioClass create(StudioClass newClass) {
        if(newClass.isValid()) {
            scheduledClasses.add(newClass);
            return newClass;
        }
        throw new StudioClassIsNotValidException();
    }

    private static Optional<StudioClass> getClassById(UUID studioClassId) {
        return scheduledClasses.stream().filter(s -> s.getId().equals(studioClassId)).findFirst();
    }

    public static Booking createBooking(UUID studioClassId, Date classDate, String memberName) {
        Optional<StudioClass> classById = getClassById(studioClassId);
        StudioClass studioClass = classById.orElseThrow(StudioClassNotFoundException::new);
        if(!studioClass.isBookingDateValid(classDate)) throw new BookingNotValidException();
        Booking booking = Booking.Create(classDate, memberName);
        studioClass.addBooking(booking);
        return booking;
    }

    private boolean isBookingDateValid(Date bookingDate) {
        return !bookingDate.before(startDate) && !bookingDate.after(endDate);
    }

    private void addBooking(Booking booking) {
        this.bookings.add(booking);
    }

    boolean isValid() {
        return this.id != null && this.className != null && this.startDate != null && this.endDate != null && this.capacity != 0;
    }
}