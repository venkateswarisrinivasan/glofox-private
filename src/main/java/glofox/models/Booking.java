package glofox.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Builder
public class Booking {

    private UUID id;

    private Date BookingDate;

    private String member;

    static Booking Create(Date classDate, String memberName) {
        return new Booking(UUID.randomUUID(), classDate, memberName);
    }
}
