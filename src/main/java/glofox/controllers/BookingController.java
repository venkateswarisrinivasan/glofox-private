package glofox.controllers;

import glofox.models.Booking;
import glofox.models.StudioClass;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

@RequestMapping("/booking")
@RestController
public class BookingController {

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Booking> book(@RequestParam("studioClassId") UUID studioClassId, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date classDate, @RequestParam String memberName) {
        try {
            Booking booking = StudioClass.createBooking(studioClassId, classDate, memberName);
            return new ResponseEntity<>(booking, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
