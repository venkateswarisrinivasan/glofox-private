package glofox.controllers;

import glofox.models.StudioClass;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/class")
public class StudioClassController {

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<StudioClass> create(@RequestBody StudioClass studioClass) {
        try {
            StudioClass scheduledClass = StudioClass.create(studioClass);
            return new ResponseEntity<>(scheduledClass, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
